% test spectra load

close all
clear
clc

addpath('Z:\Weichun\Matlab_programs\Spectra\');
folder = '..\data\NR_and_dye_spectra\';

ifsave = 1; % save the image or not?
LineWidth= 2;
Border = 2;
FontSize = 16;

%% Single NR spectrum
low = 650;
high = 845;
BG1 = load('..\data\NR_and_dye_spectra\NR771_5nM_IRD800CW_532nm_new_circular_30uW_NR_0.txt','txt');
ind1 = find(BG1(:,1)>low&BG1(:,1)<high);
wl = BG1(ind1,1);
BG = BG1(ind1,2);

NR_single = load('..\data\NR_and_dye_spectra\NR771_5nM_IRD800CW_532nm_new_circular_30uW_NR_17.txt','txt');
NR_single_net = NR_single(ind1,2)-BG;

% Response function
corr = load('..\data\spectral_correction\spectral_response.txt','txt');
ind2 = find(corr(:,1)>low&corr(:,1)<high);
response = corr(ind2,2);

NR_single_corr = NR_single_net./response; % The measured spectra and the spectrum for correction have the same range.
plot(wl,NR_single_corr/max(NR_single_corr),'c','LineWidth',LineWidth);
hold all
%% Lorentzian fit
energy_in = 1./wl;
beta0 = [1 1/780 1/740-1/800]; % Initial value for fitting % FWHM = 2*beta(3).
[beta,R,J,CovB] = nlinfit(energy_in,NR_single_corr/max(NR_single_corr),'lorentz',beta0);
spec_fit = lorentz(beta,energy_in);
SPR = 1/beta(2);
FWHM = 1/(beta(2)+beta(3))-SPR;
beta_ci = nlparci(beta,R,'jacobian',J,'alpha',0.05); % Confidence interval in the unit of 1/lambda
SPR_ci_up = 1/beta_ci(2,1) - SPR;
SPR_ci_down = SPR - 1/beta_ci(2,2);

plot(wl,spec_fit,'m','LineWidth',LineWidth);

SPR_ci_up_str = num2str(SPR_ci_up,2);
SPR_str = num2str(SPR,5);
% str_plus_minus ='$$\pm$$';
% str = strcat('{SPR = }',SPR_str,str_plus_minus,SPR_ci_up_str, '{ nm}');
% text(610, 1/2, str, 'interpreter','latex','FontSize',FontSize,'Fontname','SansSerif');

%% Bluk NR data
a = csvread(strcat(folder,'Nanopartz_A12-40-780-CTAB-replacement.csv'),2,0);
w = a(:,1);
S = a(:,2)/max(a(find(a(:,1)>400),2));

clear a
plot(w,S,'k','LineWidth',LineWidth);
% h(1).FaceColor = [218,165,32]/255;
% title('Single Rod Luminiscence')
% end
% h(1).FaceAlpha = 0.5;
clear w s

plot([785 785],[-0.1 1.1],':k','LineWidth',LineWidth) % laser line
ylim([0 1.05])
xlim([600 900])

set(gca,'Fontname','SansSerif')
set(gca,'LineWidth',Border)
set(gca,'FontSize',FontSize)
xlabel('Wavelength (nm)','FontSize',FontSize)
ylabel('Normalized intenstiy ','FontSize',FontSize)

if ifsave == 1
    saveas(gcf, '..\images\NR_spectra_single_and_bulk_matlab', 'svg');
end
