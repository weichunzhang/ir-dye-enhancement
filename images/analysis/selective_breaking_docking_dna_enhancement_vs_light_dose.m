clc
clear
close all

%% plot settings and variable definitions
LineWidth = 1;
Border = 1.5;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 1; % Whether the images are saved.
ylimit = 300;

before = dlmread('..\data\timetraces\PEG_100nM_2nW_NR6_2_timetrace_100ms.dat','',1,0);
% figure
% plot(before(:,1),before(:,2));
h = figure;
set(h,'units','centimeters')
set(h,'position',[8  2   13.5   20.5])

xleft1 = 50;
xright1 = 220;
h1 = subplot(4,1,1);
set(h1,'units','centimeters')
set(h1,'position',[2.5 17 10 3]);
plot(before(xleft1*10:xright1*10,1)-xleft1,before(xleft1*10:xright1*10,2)*100,'LineWidth',LineWidth);
set(gca,'xticklabel',[]);
ylim([0 ylimit])
xlim([0 xright1-xleft1]);

after_10uW = dlmread('..\data\timetraces\PEG_100nM_2nW_NR6_after_10uW_timetrace_100ms.dat','',1,0);
xleft2 = 125;
xright2 = 295;
h2 = subplot(4,1,2);
set(h2,'units','centimeters')
set(h2,'position',[2.5 13.5 10 3]);
plot(after_10uW(xleft2*10:xright2*10,1)-xleft2,after_10uW(xleft2*10:xright2*10,2)*100,'LineWidth',LineWidth);
set(gca,'xticklabel',[]);
ylim([0 ylimit])
xlim([0 xright2-xleft2]);
ylabel('{Fluorescence [counts / (100 ms)]}');

after_30uW = dlmread('..\data\timetraces\PEG_100nM_2nW_NR6_after_30uW_timetrace_100ms.dat','',1,0);
xleft3 = 130;
xright3 = 300;
h3 = subplot(4,1,3);
set(h3,'units','centimeters')
set(h3,'position',[2.5 10 10 3]);
plot(after_30uW(xleft3*10:xright3*10,1)-xleft3,after_30uW(xleft3*10:xright3*10,2)*100,'LineWidth',LineWidth);
ylim([0 ylimit])
xlim([0 xright3-xleft3]);
xlabel('Time (s)');

% after_100uW = dlmread('..\data\timetraces\PEG_100nM_2nW_NR6_after_100uW_timetrace_100ms.dat','',1,0);
% xleft4 = 130;
% xright4 = 300;
% subplot(4,1,4);
% plot(after_100uW(xleft4*10:xright4*10,1)-xleft4,after_100uW(xleft4*10:xright4*10,2)*100);
% % set(gca,'xticklabel',[]);
% ylim([0 ylimit])
% xlim([0 xright4-xleft4]);

A = xlsread('..\data\enhancement_vs_light_dose.xlsx',1,'B2:C24');
h4 = subplot(4,1,4);
plot(A(:,1)*0.5528/2,A(:,2),'square','MarkerFaceColor',[0.49 1 0.63]);
set(h4,'units','centimeters')
set(h4,'position',[2.5 2 10 6]);
ylabel('Irradiance (\muW x min)');
xlabel('Enhancement factor');
ylim([0 500])

set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName,'LineWidth',Border);

if ifsave == 1
    saveas(gcf,'..\images\selective_breaking_docking_dna', 'svg');
    saveas(gcf,'..\images\selective_breaking_docking_dna', 'png');
end