clc
close all
clear

%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;

%% Spectral response
low = 600;
high = 830;

real = xlsread('..\data\spectral_correction\DMANS_real_spectrum.xlsx');

A = load('..\data\spectral_correction\spectral_response_surface.txt','txt');
dark = load('..\data\spectral_correction\dark.txt','txt');

wl = dark(:,1);
ind = find(wl>=low);
measure = A(:,2)-dark(:,2);

real_interp = interp1(real(:,1),real(:,2),wl,'spline');

response = measure./real_interp;
h = figure;

set(h,'units','centimeters');
set(h,'position',[2 2 28 12]);
h1 = subplot(1,2,1);
set(h1,'units','centimeters')
set(h1,'position',[2.5 2 10 9]);
plot(wl(ind),response(ind)/max(response(ind)),'LineWidth',LineWidth);
xlim([low,high]);
ylim([0 1]);
xlabel('Wavelength (nm)');
ylabel('Relative efficiency');

%% Start

BG1 = load('..\data\NR771_water_532nm_20uW_circular_BG.txt','txt');
ind1 = find(BG1(:,1)>low&BG1(:,1)<high);
wl = BG1(ind1,1);
BG = BG1(ind1,2);

NR1 = load('..\data\NR771_water_532nm_20uW_circular_NR_16.txt','txt');
NR = NR1(ind1,2)-BG;

%% Response function
corr = load('..\data\spectral_response.txt','txt');
ind2 = find(corr(:,1)>low&corr(:,1)<high);
response = corr(ind2,2);

%% Corrected spectra
NR_corr = NR./response; % The measured spectra and the spectrum for correction have the same range.
h2 = subplot(1,2,2);
set(h2,'units','centimeters');
set(h2,'position',[16 2 11 9]);
plot(wl,NR,wl,NR_corr,'LineWidth',LineWidth);
xlim([low high]);
ylim([min(NR_corr) - 10 max(NR_corr) + 10]);

hold all

%% Lorentzian fit
energy_in = 1./wl;
beta0 = [max(NR_corr) 1/780 1/740-1/800]; % Initial value for fitting % FWHM = 2*beta(3).
[beta,R,J,CovB] = nlinfit(energy_in,NR_corr,'lorentz',beta0);
spec_fit = lorentz(beta,energy_in);
SPR = 1/beta(2);
FWHM = 1/(beta(2)+beta(3))-SPR;
beta_ci = nlparci(beta,R,'jacobian',J,'alpha',0.05); % Confidence interval in the unit of 1/lambda
SPR_ci_up = 1/beta_ci(2,1) - SPR;
SPR_ci_down = SPR - 1/beta_ci(2,2);

plot(wl,spec_fit,'g','LineWidth',LineWidth);

legend('Raw spectrum','Corrected spectrum','Lorentz fit','Location','Northwest');
xlabel('Wavelength (nm)');
ylabel('Intensity (a.u.)');

SPR_ci_up_str = num2str(SPR_ci_up,2);
SPR_str = num2str(SPR,5);
str_plus_minus ='$$\pm$$';
str = strcat('{SPR = }',SPR_str,str_plus_minus,SPR_ci_up_str, '{ nm}');
text(610, max(NR_corr)/2, str, 'interpreter','latex','FontSize',FontSize,'Fontname','SansSerif');

set(h1,'FontSize',FontSize)
set(h1,'lineWidth',Border)
set(h1,'Fontname','SansSerif')
set(h2,'FontSize',FontSize)
set(h2,'Fontname','SansSerif')
set(h2,'lineWidth',Border)

saveas(gcf, '..\spectral_correction', 'svg');
saveas(gcf, '..\spectral_correction', 'png');