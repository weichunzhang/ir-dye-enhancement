%%%%%%%%%%%%%%%%%%% Extracting PSF sizes %%%%%%%%%%%%%%%%%%%%%%
% This program plots and calculates the PSF measured with Adwin.
% Three images for x-y, y-z and x-z planes are recorded with high
% resolution.
% By M. Caldarola and W. Zhang.
%%
clear
close all
clc

%% Output settings
y_lim = [46.56 47.52];
z_lim = [47.1 49.69];
LineWidth = 1.5;
FontSize = 16;
Boarder = 1.5;
MarkerSize = 10;
FontName = 'SansSerif';
ifsave = 1;

%%
folder = '..\data\PSF\';
file = 'NR771_water_785nm_CW_2676uW_measured_yz.dat';

% center = [53.52 47.03]; % xy
% center = [53.57, 48.2]; % xz
center = [47.03 48.2]; % yz plane
dims= [1, 3];
accuracy = [0.02, 0.02];
pixel = dims./accuracy; % Number of pixels
int_time = 0.01; % Integration time;
x = center(1)-dims(1)/2:accuracy(1):center(1)+dims(1)/2-accuracy(1);
y = center(2)-dims(2)/2:accuracy(2):center(2)+dims(2)/2-accuracy(2);
%% read data
disp(strcat('Loading data:',file))
tic
A = dlmread(strcat(folder,file),',',4,0);
toc

data_nir = A(1:pixel(2),:); % Counts per millisecond

      %% 2D Guassian fit
    
    x0 = [40,center(1),0.2,center(2),0.3,0.01]; %Inital guess parameters
    [X,Y] = meshgrid(x,y);
    xdata = zeros(size(X,1),size(Y,2),2);
    xdata(:,:,1) = X;
    xdata(:,:,2) = Y;
    [beta,resnorm,residual,exitflag,output,lambda,J] = lsqcurvefit(@D2GaussFunctionRot,x0,xdata,data_nir);
    beta_ci = nlparci(beta,residual,'jacobian',J); % confidence interval.
    %% plot
    hf1 = figure(2);
    set(hf1, 'Position', [50 50 600 900])
    alpha(0)
    subplot(3,2, [3,5])
    imagesc(x,y,data_nir(:,:));
    axis('equal');
    colormap('jet')
    xlabel('y (\mum)')
    ylabel('z (\mum)')
    set(gca,'Ydir','Normal')
    xlim(y_lim);
    ylim(z_lim);

   % string1 = ['Amplitude','             \mu_y', '        \sigma_y','        \mu_z','      \sigma_z','          Angle'];
   % string2 = [num2str(beta(1), '% 100.3f'),'      ',num2str(beta(2), '% 100.3f'),'    ',num2str(beta(3), '% 100.3f'),'       ',num2str(beta(4), '% 100.3f'),'      ',num2str(beta(5), '% 100.3f'),'   ',num2str(180*beta(6)/pi, '% 100.3f'),'^o'];

   % text(center(1)-dims(1)/2+0.1,center(2)-dims(2)/2+0.3,string1,'Color','red')
   % text(center(1)-dims(1)/2+0.1,center(2)-dims(2)/2+0.2,string2,'Color','red')

    %% -----Calculate cross sections-------------
    % generate points along horizontal axis
    InterpolationMethod = 'nearest'; % 'nearest','linear','spline','cubic'
    m = -tan(beta(6));% Point slope formula
    b = (-m*beta(2) + beta(4));
    xvh = x;
    yvh = xvh*m + b;
    hPoints = interp2(X,Y,data_nir,xvh,yvh,InterpolationMethod);
    % generate points along vertical axis
    mrot = -m;
    brot = (mrot*beta(4) - beta(2));
    yvv = y;
    xvv = yvv*mrot - brot;
    vPoints = interp2(X,Y,data_nir,xvv,yvv,InterpolationMethod);

%     hold on % Indicate major and minor axis on plot
% 
%     % % plot pints 
% %     plot(xvh,yvh,'r.') 
% %     plot(xvv,yvv,'g.')
% 
%     % plot lins 
%     plot([xvh(1) xvh(length(xvh))],[yvh(1) yvh(length(yvh))],'r') 
%     plot([xvv(1) xvv(length(xvv))],[yvv(1) yvv(length(yvv))],'g') 
% 
%     hold off
    
    xdatafit = linspace(x(1),x(end),300);
    ydatafit = linspace(y(1),y(end),300);
    hdatafit = beta(1)*exp(-(xdatafit-beta(2)).^2/(2*beta(3)^2));
    vdatafit = beta(1)*exp(-(ydatafit-beta(4)).^2/(2*beta(5)^2));
    h1 = subplot(3,2,1);
    xposh = (xvh-beta(2))/cos(beta(6))+beta(2);% correct for the longer diagonal if fi~=0
    plot(xposh,hPoints,'r.',xdatafit,hdatafit,'black','LineWidth',LineWidth,'MarkerSize',MarkerSize)
    ylabel('{counts / ms}');
    xlim(y_lim);
    % set(h1,'position','
    set(gca,'LineWidth',Boarder);
    h2 = subplot(3,2,[4,6]);
    xposv = (yvv-beta(4))/cos(beta(6))+beta(4);% correct for the longer diagonal if fi~=0
    plot(vPoints,xposv,'g.',vdatafit,ydatafit,'black','LineWidth',LineWidth,'MarkerSize',MarkerSize)
    xlabel('{counts / ms}');
    ylim(z_lim);
    set(gca,'LineWidth',Boarder);
    figure(gcf) % bring current figure to front
    
    %% Output settings

set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);
%% Image export
if ifsave == 1
    saveas(gcf, '..\images\PSF_yz_matlab', 'svg');
    saveas(gcf, '..\images\PSF_yz_matlab', 'png');
    saveas(gcf, '..\images\PSF_yz', 'pdf');
end