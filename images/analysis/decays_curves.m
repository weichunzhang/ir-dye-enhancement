close all
clear
clc
%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 0;

A = xlsread('..\data\Decay_curves\MPD_glass_decay_as_IRF.xlsx',1,'A10:C1571');
tau_1 = A(:,1);
IRF = A(:,3);

B = xlsread('..\data\Decay_curves\A1p1_60%_burst_2_decay.xlsx',1,'A12:C1574');
tau_2 = B(:,1);
decay = B(:,3);

figure;
semilogy(tau_1+0.18,IRF/max(IRF),'k',tau_2,decay/max(decay),'r.','LineWidth',LineWidth);
xlim([7 12.5]);
% legend('IRF','Data','3 exp. fit');
% title('QD in water');
xlabel('Time (ns)');
ylabel('Norm. # of counts');

set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName,'LineWidth',Border)

%% Save the figure
if ifsave == 1
    saveas(gcf, '..\images\decay_bursts', 'svg')
    saveas(gcf, '..\images\decay_bursts', 'png')
end