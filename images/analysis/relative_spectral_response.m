clc
close all
clear

%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;

%% Spectral response
low = 600;
high = 830;

real = xlsread('..\data\spectral_correction\DMANS_real_spectrum.xlsx');

A = load('..\data\spectral_correction\spectral_response_surface.txt','txt');
dark = load('..\data\spectral_correction\dark.txt','txt');

wl = dark(:,1);
ind = find(wl>=low);
measure = A(:,2)-dark(:,2);

real_interp = interp1(real(:,1),real(:,2),wl,'spline');

response = measure./real_interp;
h = figure;

set(h,'units','centimeters');
set(h,'position',[2 2 15 12]);

plot(wl(ind),response(ind)/max(response(ind)),'LineWidth',LineWidth);
xlim([low,high]);
ylim([0 1]);
xlabel('Wavelength (nm)');
ylabel('Relative efficiency');

set(gca,'FontSize',FontSize)
set(gca,'lineWidth',Border)
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'Fontname','SansSerif')
set(gca,'lineWidth',Border)

saveas(gcf, '..\images\relative_spectral_response_matlab', 'svg');
saveas(gcf, '..\images\relative_spectral_response_matlab', 'png');