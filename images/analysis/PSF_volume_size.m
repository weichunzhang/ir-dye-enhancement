clear
close all
clc

%% x
x1 = 320.8;
x1_err = 4.6;
x2 = 326.6;
x2_err = 2.8;

x = (x1+x2)/2;
x_err = sqrt((x1_err/2)^2+(x2_err/2)^2);

%% y
y1 = 275.6;
y1_err = 3.8;
y2 = 299.8;
y2_err = 2.6;

y = (y1+y2)/2;
y_err = sqrt((y1_err/2)^2+(y2_err/2)^2);

%% z
z1 = 873.2;
z1_err = 7.8;
z2 = 912.4;
z2_err = 8;

z = (z1+z2)/2;
z_err = sqrt((z1_err/2)^2+(z2_err/2)^2);

Vconf = (pi/2)^1.5*x*y*z
Vconf_err = Vconf*sqrt((x_err/x)^2 + (y_err/y)^2 + (z_err/z)^2)

Veff = pi^1.5*x*y*z
Veff_err = Veff*sqrt((x_err/x)^2 + (y_err/y)^2 + (z_err/z)^2)

c = 100; % concentration, nM
NA = 6.02214086e23; % avogadro constant
N = c*1e-9*Vconf*1e-24*NA; % number of molecules in the focal volume.
N_err = c*1e-9*Vconf_err*1e-24*NA; % uncertainty.

pre_2nW = 19.6650;
pre_err = 3.0541; % count rate from the solution.

mo_br = pre_2nW/N % molecular brightness
mo_br_err = mo_br*sqrt((pre_err/pre_2nW)^2 + (N_err/N)^2)