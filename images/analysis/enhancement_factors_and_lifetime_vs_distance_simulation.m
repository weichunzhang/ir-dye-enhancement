close all
clear
clc
%% Define parameters
LineWidth = 2;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 0; % Whether the images are saved.
QY = 0.07;
tau0 = 0.55;
d = 1:1:15;

%% Reading data and calculate
E = load('..\data\excitation_enhancement\linear_38x116_electric_field_norm.txt','txt'); % Electric field enhancement
E_exc = E(:,4).^2;
rates = dlmread('..\data\emission_enhancement\EnhancedRate_x.dat','',1,1); % Only the parallel orientation is considered.
kr = rates(:,1); % radiative enhancement
Knr = rates(:,2); % quenching

E_em = kr./(QY*(kr + Knr + 1/QY -1));
tau = tau0./(QY*(kr + Knr + 1/QY -1));

E_all = E_exc.*E_em; % Overall enhancement
%% Plot
h = figure(1); % Excitation and emission enhancement
h1 = subplot(1,2,1);
plot(d,E_exc,'o-','LineWidth',LineWidth);
xlabel('Distance (nm)');
ylabel('Excitation enhancement');
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)
h2 = subplot(1,2,2);
plot(d,E_em,'o-','LineWidth',LineWidth);
xlabel('Distance (nm)');
ylabel('Emission enhancement');
% plot settings
set(h,'units','centimeters')
set(h,'position',[10 5 24 10])
set(h1,'units','centimeters');
set(h2,'units','centimeters');
set(h1,'position',[2 2 8 7]);
set(h2,'position',[15 2 8 7]);
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)


H = figure(2); % Overall enhancement factor and new lifetime
H1 = subplot(1,2,1)
plot(d,E_all,'o-','LineWidth',LineWidth);
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)
xlabel('Distance (nm)');
ylabel('Overall enhancement');
H2 = subplot(1,2,2);
plot(d,tau,'o-','LineWidth',LineWidth);
xlabel('Distance (nm)');
ylabel('Modified lifetime (ns)');
set(H,'units','centimeters')
set(H,'position',[10 5 24 10])
set(H1,'units','centimeters');
set(H2,'units','centimeters');
set(H1,'position',[2 2 8 7]);
set(H2,'position',[15 2 8 7]);
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)

figure
plot(d,kr,d,Knr);
