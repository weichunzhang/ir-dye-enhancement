% Absorption and emission spectrum of the dye.
close all
clear
clc

addpath('Z:\Weichun\Matlab_programs\Spectra\');
folder = '..\data\NR_and_dye_spectra\';

ifsave = 1; % save the image or not?
LineWidth= 2;
Border = 2;
FontSize = 16;

%% Dye spectra
% absorption
absorption = dlmread(strcat(folder,'IRDye800CW_imager_500nM_absorption.csv'),',',2,0);
% emission
emission = dlmread(strcat(folder,'IRDye800CW_imager_50nM_760ex_high_slow.csv'),',',2,0);
ind3 = find(absorption(:,1)>500 & absorption(:,1)<900);

%% Plot
plot(absorption(:,1),absorption(:,2)./max(absorption(ind3,2)),'b',emission(:,1),emission(:,2)./max(emission(:,2)),'r--','LineWidth',LineWidth);
% wl,NR_single_corr/max(NR_single_corr),'g',
hold all
% plot(wa,a./max(a),'color',[0 0.5 1],'LineWidth',LineWidth);
% If sinle rod spectrum is added, it is too crowded.
plot([785 785],[-0.1 1.1],':k','LineWidth',LineWidth) % laser line
ylim([-0.02 1.05])
xlim([600 900])

set(gca,'Fontname','SansSerif')
set(gca,'LineWidth',Border)
set(gca,'FontSize',FontSize)
xlabel('Wavelength (nm)','FontSize',FontSize)
ylabel('Normalized intenstiy ','FontSize',FontSize)

if ifsave == 1
    saveas(gcf, '..\images\dye_spectra_matlab', 'svg');
end
