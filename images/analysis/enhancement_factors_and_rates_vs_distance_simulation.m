close all
clear
clc
%% Define parameters
LineWidth = 2;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 1; % Whether the images are saved.
QY = 0.07;
tau0 = 0.55;
d = 1:1:15;
%% color def

col(1,:)=[0    0.4470    0.7410];
col(2,:)=[0.8500    0.3250    0.0980];
col(3,:)=[0.9290    0.6940    0.1250];
col(4,:)=[0.4940    0.1840    0.5560];

col(5,:)=[255   133    74]/255;
col(6,:)=[0.4660    0.6740    0.1880];
col(7,:)=[0.3010    0.7450    0.9330];

%% Reading data and calculate
E = load('..\data\excitation_enhancement\linear_38x116_electric_field_norm.txt','txt'); % Electric field enhancement
E_exc = E(:,4).^2;
rates = dlmread('..\data\emission_enhancement\EnhancedRate_x.dat','',1,1); % Only the parallel orientation is considered.
rates1 = dlmread('..\data\emission_enhancement\EnhancedRate_x_one_more_point.dat','',1,0); % d = 1.5 nm is also calculated
kr = rates(:,1); % radiative enhancement
Knr = rates(:,2); % quenching
d1 = rates1(:,1);
kr1 = rates1(:,2);
Knr1 = rates1(:,3);

E_em = kr./(QY*(kr + Knr + 1/QY -1));
tau = tau0./(QY*(kr + Knr + 1/QY -1));

E_all = E_exc.*E_em; % Overall enhancement
%% Plot
h = figure(1); % Excitation and emission enhancement
h1 = subplot(1,2,1);
plot(d1,kr1,'bo-','LineWidth',LineWidth,...
    'MarkerSize',8,'MarkerEdgeColor','b','MarkerFaceColor','w');
hold all
plot(d1,Knr1,'rs-','LineWidth',LineWidth,...
    'MarkerSize',8,'MarkerEdgeColor','r','MarkerFaceColor','w');
xlabel('Distance (nm)');
ylabel('Rate enhancement');
l = legend({'$k_{r}$/$k^0_r$','$K_{nr}$/$k^0_r$'},'interpreter','latex');


h2 = subplot(1,2,2);
yyaxis left
plot(d,E_exc,'^-',...
    'LineWidth',LineWidth,'color',col(5,:),...
    'MarkerSize',8,'MarkerEdgeColor',col(5,:),'MarkerFaceColor','w'); %
xlabel('Distance (nm)')
ylabel('Excitation enhancement','color','blue');
set(h2,'YColor',col(5,:));

yyaxis right
plot(d,E_em,'d-',...
    'LineWidth',LineWidth,'color',col(6,:),...
    'MarkerSize',8,'MarkerEdgeColor',col(6,:),'MarkerFaceColor','w'); %
ylabel('Emission enhancement');
set(h2,'YColor',col(6,:));
% plot settings
set(h,'units','centimeters')
set(h,'position',[10 5 22.5 9])
set(h1,'units','centimeters');
set(h2,'units','centimeters');
set(h1,'position',[2.5 2 7 6]);
set(h2,'position',[13.5 2 7 6]);
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName,'LineWidth',Border);

set(l,'fontsize',FontSize + 5); % Large fontsize for the lengend.

if ifsave == 1
    saveas(gcf,'..\images\enhancement_factors_and_rates_vs_distance_simulation_matlab','svg');
end
