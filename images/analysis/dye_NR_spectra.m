% test spectra load

close all
clear
clc

addpath('Z:\Weichun\Matlab_programs\Spectra\');
folder = '..\data\NR_and_dye_spectra\';

ifsave = 0; % save the image or not?
LineWidth= 2;
Border = 2;
FontSize = 16;

%% Single NR spectrum
low = 600;
high = 845;
BG1 = load('..\data\NR_and_dye_spectra\NR771_5nM_IRD800CW_532nm_new_circular_30uW_NR_0.txt','txt');
ind1 = find(BG1(:,1)>low&BG1(:,1)<high);
wl = BG1(ind1,1);
BG = BG1(ind1,2);

NR_single = load('..\data\NR_and_dye_spectra\NR771_5nM_IRD800CW_532nm_new_circular_30uW_NR_17.txt','txt');
NR_single_net = NR_single(ind1,2)-BG;

% Response function
corr = load('..\data\spectral_correction\spectral_response.txt','txt');
ind2 = find(corr(:,1)>low&corr(:,1)<high);
response = corr(ind2,2);

NR_single_corr = NR_single_net./response; % The measured spectra and the spectrum for correction have the same range.

%% Bluk NR data
a = csvread(strcat(folder,'Nanopartz_A12-40-780-CTAB-replacement.csv'),2,0);
w = a(:,1);
S = a(:,2)/max(a(find(a(:,1)>400),2));

clear a
plot(w,S,'k','LineWidth',LineWidth);
% h(1).FaceColor = [218,165,32]/255;
% title('Single Rod Luminiscence')
hold all
% end
% h(1).FaceAlpha = 0.5;
clear w s

%% Dye spectra
% absorption
absorption = dlmread(strcat(folder,'IRDye800CW_imager_500nM_absorption.csv'),',',2,0);
% emission
emission = dlmread(strcat(folder,'IRDye800CW_imager_50nM_760ex_high_slow.csv'),',',2,0);
ind3 = find(absorption(:,1)>500 & absorption(:,1)<900);

%% Plot
plot(absorption(:,1),absorption(:,2)./max(absorption(ind3,2)),'b',emission(:,1),emission(:,2)./max(emission(:,2)),'r--','LineWidth',LineWidth);
% wl,NR_single_corr/max(NR_single_corr),'g',
hold all
% plot(wa,a./max(a),'color',[0 0.5 1],'LineWidth',LineWidth);
% If sinle rod spectrum is added, it is too crowded.
plot([785 785],[-0.1 1.1],':k','LineWidth',LineWidth) % laser line
ylim([0 1.05])
xlim([450 900])

set(gca,'Fontname','SansSerif')
set(gca,'LineWidth',Border)
set(gca,'FontSize',FontSize)
xlabel('Wavelength (nm)','FontSize',FontSize)
ylabel('Normalized intenstiy ','FontSize',FontSize)

if ifsave == 1
    saveas(gcf, '..\images\dye_and_NR_spectra_matlab', 'svg');
end
