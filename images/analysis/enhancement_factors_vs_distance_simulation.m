close all
clear
clc
%% Define parameters
LineWidth = 2;
Border = 1.5;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 1; % Whether the images are saved.
QY = 0.07;

d = 1:1:15;

%% Reading data and calculate
E = load('..\data\excitation_enhancement\linear_38x116_electric_field_norm.txt','txt'); % Electric field enhancement
E_exc = E(:,4).^2;
rates = dlmread('..\data\emission_enhancement\EnhancedRate_x.dat','',1,1); % Only the parallel orientation is considered.
kr = rates(:,1); % radiative enhancement
Knr = rates(:,2); % quenching

E_em = kr./(QY*(kr + Knr + 1/QY -1));
E_all = E_exc.*E_em; % Overall enhancement

plot(d,E_all,'o','MarkerSize',8,'MarkerFaceColor','w','LineWidth',LineWidth);
xlabel('Distance (nm)');
ylabel('Overall enhancement');
hold all

d_interp = 0.8:0.1:15;
E_interp = interp1(d,E_all,d_interp,'spline');
plot(d_interp,E_interp,'r--','LineWidth',LineWidth);

set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName,'LineWidth',Border);
if ifsave == 1
    saveas(gcf,'..\images\overall_enhancement_vs_distance_matlab', 'svg');
    saveas(gcf,'..\images\overall_enhancement_vs_distance_matlab', 'png');
end
