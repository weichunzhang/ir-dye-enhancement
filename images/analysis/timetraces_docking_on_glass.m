clc
close all
clear

%% plot settings and variable definitions
LineWidth = 1;
Border = 1.5;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 0; % Whether the images are saved.
ylimit = 800;
%% plots

before = dlmread('..\data\timetraces\NR17_2nW_timetrace_100ms.dat','',1,0);
xleft1 = 0.1;
xright1 = 300;
h1 = subplot(2,1,1);
plot(before(xleft1*10:xright1*10,1)-xleft1,before(xleft1*10:xright1*10,2)*100,'LineWidth',LineWidth); % counts / (100 ms)
% set(gca,'xticklabel',[]);
% xlabel('Time (s)');
ylim([0 ylimit])
xlim([0 xright1-xleft1]);
set(gca,'ytick',[250 500 750]);

after = dlmread('..\data\timetraces\NR17_2nW_after_100uW_timetrace_100ms.dat','',1,0);
xleft2 = 145.1;
xright2 = 445;
h2 = subplot(2,1,2);
plot(after(xleft2*10:xright2*10,1)-xleft2,after(xleft2*10:xright2*10,2)*100,'LineWidth',LineWidth);

ylim([0 ylimit])
xlim([0 xright2-xleft2]);
set(gca,'ytick',[250 500 750]);
xlabel('Time (s)');
p1=get(h1,'position');
p2=get(h2,'position');
height=p1(2)+p1(4)-p2(2);
h3=axes('position',[p2(1) p2(2) p2(3) height],'visible','off');
h_label=ylabel('{Fluorescence [counts / (100 ms)]}','visible','on');

set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName,'LineWidth',Border)

if ifsave == 1
    saveas(gcf,'..\images\timetraces_docking_on_glass_matlab', 'svg');
    saveas(gcf,'..\images\timetraces_docking_on_glass_matlab', 'png');
end