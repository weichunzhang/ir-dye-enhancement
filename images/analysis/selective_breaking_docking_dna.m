clc
clear
close all

%% plot settings and variable definitions
LineWidth = 1;
Border = 1.5;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 0; % Whether the images are saved.
ylimit = 300;

before = dlmread('..\data\timetraces\PEG_100nM_2nW_NR6_2_timetrace_100ms.dat','',1,0);
% figure
% plot(before(:,1),before(:,2));

xleft1 = 50;
xright1 = 220;
subplot(3,1,1);
plot(before(xleft1*10:xright1*10,1)-xleft1,before(xleft1*10:xright1*10,2)*100,'LineWidth',LineWidth);
set(gca,'xticklabel',[]);
ylim([0 ylimit])
xlim([0 xright1-xleft1]);

after_10uW = dlmread('..\data\timetraces\PEG_100nM_2nW_NR6_after_10uW_timetrace_100ms.dat','',1,0);
xleft2 = 125;
xright2 = 295;
h1 = subplot(3,1,2);
plot(after_10uW(xleft2*10:xright2*10,1)-xleft2,after_10uW(xleft2*10:xright2*10,2)*100,'LineWidth',LineWidth);
set(gca,'xticklabel',[]);
ylim([0 ylimit])
xlim([0 xright2-xleft2]);
ylabel('{Fluorescence [counts / (100 ms)]}');

after_30uW = dlmread('..\data\timetraces\PEG_100nM_2nW_NR6_after_30uW_timetrace_100ms.dat','',1,0);
xleft3 = 130;
xright3 = 300;
h2 = subplot(3,1,3);
plot(after_30uW(xleft3*10:xright3*10,1)-xleft3,after_30uW(xleft3*10:xright3*10,2)*100,'LineWidth',LineWidth);
set(gca,'xticklabel',[]);
ylim([0 ylimit])
xlim([0 xright3-xleft3]);

% after_100uW = dlmread('..\data\timetraces\PEG_100nM_2nW_NR6_after_100uW_timetrace_100ms.dat','',1,0);
% xleft4 = 130;
% xright4 = 300;
% subplot(4,1,4);
% plot(after_100uW(xleft4*10:xright4*10,1)-xleft4,after_100uW(xleft4*10:xright4*10,2)*100);
% % set(gca,'xticklabel',[]);
% ylim([0 ylimit])
% xlim([0 xright4-xleft4]);

xlabel('Time (s)');
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName,'LineWidth',Border);

if ifsave == 1
    saveas(gcf,'..\images\selective_breaking_docking_dna', 'svg');
    saveas(gcf,'..\images\selective_breaking_docking_dna', 'png');
end