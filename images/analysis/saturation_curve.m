close all
clear
clc
%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;

%% Read data
folder = '..\data\saturation_curve\';

bg = dlmread(strcat(folder,'dark.dat'),',',3,1);

bkg_mean = mean(bg);
bkg_std = std(bg);

file = dir(strcat(folder,'Power_dependence_Sample=100nM_IRD800CW_imager_785nm_circular_lambda=785nm_Meas_at_15hs50m_averaged.dat'));

a = csvread(strcat(folder,file.name),1,0);
pw = a(:,1)*90/20; % power at bfp

c2 = a(:,3); % counter 2
e2 = a(:,5);

ch2 = c2 - bkg_mean;
e = sqrt(e2.^2+bkg_std.^2); % propagation of uncertainty.

errorbar(pw,ch2,e,'sb','LineWidth',LineWidth);
hold all
%% fit
power_min = 0.01; % The lower power limit for fitting
power_max = 15; % The upper power limit for fitting
log_power = log(pw(find(pw > power_min & pw < power_max))); % Power for fitting
log_cnt = log(ch2(find(pw > power_min & pw < power_max))); % counts for fitting.

[p_poly,S] = polyfit(log_power,log_cnt,1); % mu improves the numerical properties of both the polynomial and the fitting algorithm.
cnt_predict = polyval(p_poly,log_power);
yresid = log_cnt - cnt_predict; % Redidual
SSresid = sum(yresid.^2);
SStotal = (length(log_cnt)-1) * var(log_cnt);
rsq = 1 - SSresid/SStotal; % R squared.
plot(exp(log_power),exp(cnt_predict),'r--','LineWidth',LineWidth);
xlim([0.01 200]);
ylim([1e2 3e5]);

set(gca,'yscale','log')
set(gca,'xscale','log')
legend('Data',strcat('{Fit. Slope = }',num2str(p_poly(1))));
legend('location','southeast')
xlabel('Excitation power(\muW)')
ylabel('Fluorescence (counts/s)')
set(gca,'xtick',[1e-2 1e-1 1 10 100]);
set(gca,'ytick',[1e2 1e3 1e4 1e5 1e6]);
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)
grid
saveas(gcf, '..\images\saturation_curve', 'png')
saveas(gcf, '..\images\saturation_curve', 'svg')

%% Prediction at 2 nW
[Y delta] = polyval(p_poly,log(0.002),S);
pre_2nW = exp(Y)
pre_err = pre_2nW*delta

% %%  I fit the traces with Poissonian. Analyze the background time trace separately
% figure(2)
% histfit(bg,[],'poisson')
% pd_bg = fitdist(bg,'poisson');
% xlabel('photons/10ms');
% ylabel('Occurence');
% title('Dark counts');
% bg_center = pd_bg.lambda; % Convert to counts/s.
% bg_ci = paramci(pd_bg); % Confidence interval with alpha = 0.05.
% bg_err = bg_ci(2)-bg_center;

% %% In order to get rid of the burst and calculate more accurately the mean counts, I fit the traces with Poissonian.
% % Now the time traces with laser on.
% files = dir(strcat(folder,'Power_dependence_Sample=100nM_IRD800CW_imager_785nm_circular_lambda=785nm_Meas_at_15hs50m_TimeTrace_Power=*.*.dat'));
% % Read only the time traces with laser on.
% 
% for N = 1:size(files,1)
%     data.a = csvread(strcat(folder,files(N).name),2,0);
%     aux1 = strfind(files(N).name,'='); % Find the power number
%     aux2 = strfind(files(N).name,'u'); % Find the power number
%     data.power(N) = str2double(files(N).name(aux1(end)+1:aux2(end)-1))/20;
%     h = figure(N+2);
%     set(h,'visible','off');
%     histfit(data.a',[],'poisson')
%     data.pd(N) = fitdist(data.a','poisson');
%     xlabel('photons/10ms');
%     ylabel('Occurence');
%     title(strcat(num2str(data.power(N)),'\muW'));
%     
%     data.cnt_center(N) = data.pd(N).lambda*100 - bg_center;
%     cnt_ci = paramci(data.pd(N))*100;
%     data.cnt_var(N) = cnt_ci(2) - data.pd(N).lambda*100;% Without error propagation.
%     data.cnt_err(N) = sqrt(bg_err^2+data.cnt_var(N)^2); % With error propagation.
%     
%     clear aux1 aux2 data.a cnt_ci
% end
% 
% %% Plot the power dependence curve with errorbars.
% figure(N+3)
% power_min = 0.01; % The lower power limit for fitting
% power_max = 15; % The upper power limit for fitting
% errorbar(data.power,data.cnt_center,data.cnt_err,'s','LineWidth',LineWidth);
% set(gca,'yscale','log')
% set(gca,'xscale','log')
% hold on
% 
% %% Now do linear fit
% fit_power = data.power(find(data.power>power_min & data.power<power_max)); % Power for fitting
% fit_cnt = data.cnt_center(find(data.power>power_min & data.power<power_max)); % counts for fitting.
% p = polyfit(fit_power,fit_cnt,1);
% cnt_predict = polyval(p,fit_power);
% yresid = fit_cnt - cnt_predict; % Redidual
% SSresid = sum(yresid.^2);
% SStotal = (length(fit_cnt)-1) * var(fit_cnt);
% rsq = 1 - SSresid/SStotal; % R squared.
% plot(fit_power,cnt_predict,'r--');

% set(gcf,'units','centimeters')
% set(gcf,'position',[5 5 16 12]); % set(gcf,'position',[left bottom width height]) 
% set(gca,'Fontname','SansSerif')
% set(gca,'FontSize',FontSize)
% set(gca,'linewidth',Border)

% saveas(gcf, '..\QD_two_photon_power_dependence', 'png')
% saveas(gcf, '..\QD_two_photon_power_dependence', 'svg')
