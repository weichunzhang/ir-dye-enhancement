clc
clear
close all

%% Read data
AA=load('..\data\excitation_enhancement\linear_38x116_electric_field_norm_profile.txt','txt');
x=AA(:,1);y=AA(:,2);I=AA(:,4).^2;
F = scatteredInterpolant(x,y,I);
[xx,yy]=meshgrid(linspace(-60e-9,60e-9,600),linspace(-100e-9,100e-9,1000));
NORME=F(xx,yy);

%% Output settings
LineWidth = 2;
FontSize = 16;
Boarder = 2;
FontName = 'SansSerif';
ifsave = 1;

%% PLot the profile
h = surf(xx,yy,NORME);
shading interp
view(0,90)
c = colorbar;
c.Label.String = '|\bf{E}|^2/|\bf{E}_0|^2';
colormap hot
xticks([]);
yticks([]);
axis image
hold on

%% Add a scale bar
z_max = max(max(get(h,'Zdata')));
scalebar_x = [30e-9 50e-9];
scalebar_y = [-85e-9 -85e-9];
l = line(scalebar_x,scalebar_y,z_max*ones(1,2));
set(l,'Linewidth',LineWidth,'Color','white');
t = text(25e-9,-75e-9,z_max,'20 nm');
set(t,'Linewidth',LineWidth,'Color','white');
hold off

%% Output settings
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName)

%% Image export
if ifsave == 1
    saveas(gcf, '..\images\intensity_profile_matlab', 'svg');
    saveas(gcf, '..\images\intensity_profile_matlab', 'png');
end
 